<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Recuperar Contraseña-DB</title>
        <link REL="Shortcut Icon" HREF="images/icono.png">
        <link href="css/metro-bootstrap.css" rel="stylesheet">
        <link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
        <link href="js/prettify/prettify.css" rel="stylesheet">

        <!-- Load JavaScript Libraries -->
        <script src="js/jquery/jquery.min.js"></script>
        <script src="js/jquery/jquery.widget.min.js"></script>
        <script src="js/jquery/jquery.mousewheel.js"></script>
        <script src="js/prettify/prettify.js"></script>
        <script src="js/metro.min.js"></script>
        <script src="js/funciones.js"></script>
        <link rel="stylesheet" type="text/css" href="css/csslogin.css">
    </head>
    <body class="metro bg-body">
        <div class="content_page">
            <div class="row">
                <div class="column grid_12 bg-captura-user mar-top-content">
                    <div class="bg-slider-login">
                        <div class="mar-title-login">
                            <h2>
                                Recuperar Contraseña<small class="on-right">Digital-Books</small>
                            </h2>
                        </div>
                        <?php
                            include("config.php"); /*Archivos de configuración de la bases de datos*/
                            if (isset($_GET["userexist"])){
                                if ($_GET["userexist"]=="no"){
                                    echo "<div class=\"color-error-sesion\"><p>El usuario o clave de administrador no existe.</p></div>";
                                }
                            }
                        ?>
                        <div class="mar-form-login">
                            <form action="guardarpasswd.php" method="post" onSubmit="return noCampoPasswdNueva(this);" onKeyUp="calcLong('pass',this,20); calcLong('log',this, 20);">
                                <div class="tam-cont-input">
                                    <div class="input-control text">
                                        <input name="log" type="text" value="<?php if (isset($_GET["log"])){ echo $_GET["log"];}?>" placeholder="Usuario" onkeypress="return validarNumLetras(event);"/>
                                        <button class="btn-clear"></button>
                                    </div>
                                    <div class="input-control text">
                                        <input name="clave" type="text" value="<?php if (isset($_GET["clave"])){ echo $_GET["clave"];}?>" placeholder="Clave de administrador" onkeypress="return validarNumLetras(event);"/>
                                        <button class="btn-clear"></button>
                                    </div>
                                    <div class="input-control password">
                                        <input name="pass" type="password" value="<?php if (isset($_GET["pass"])){ echo $_GET["pass"];}?>" placeholder="Contraseña"/>
                                        <button class="btn-reveal"></button>
                                    </div>
                                    <div class="input-control password">
                                        <input name="pass1" type="password" value="<?php if (isset($_GET["pass1"])){ echo $_GET["pass1"];}?>" placeholder="Repetir Contraseña"/>
                                        <button class="btn-reveal"></button>
                                        <div class="pos-error-registros"> 
                                            <p><?php if (isset($_GET["error"])){
                                                    if($_GET["error"]=="no"){
                                                        echo "Las contraseñas no coinciden.";   
                                                    }
                                                    /*header("refresh:2; url=index.php");*/
                                                }?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="pos-bot-recup-passwd">
                                    <a href="index.php" class="button link bg-pass"><p>Atrás</p></a>
                                </div>
                                <div class="alig-bot-recup-passwd">
                                    <input type="submit" name="Submit" value="Cambiar" class="button bg-pass button large primary">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>