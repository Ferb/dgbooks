<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Registrarse-DB</title>
        <link REL="Shortcut Icon" HREF="images/icono.png">
        <link href="css/metro-bootstrap.css" rel="stylesheet">
        <link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
        <link href="js/prettify/prettify.css" rel="stylesheet">

        <!-- Load JavaScript Libraries -->
        <script src="js/jquery/jquery.min.js"></script>
        <script src="js/jquery/jquery.widget.min.js"></script>
        <script src="js/jquery/jquery.mousewheel.js"></script>
        <script src="js/prettify/prettify.js"></script>
        <script src="js/metro.min.js"></script>
        <script src="js/funciones.js"></script>
        <link rel="stylesheet" type="text/css" href="css/csslogin.css">
    </head>
    <body class="metro bg-body">
        <div class="content_page">
            <div class="row">
                <div class="column grid_12 bg-captura-user mar-top-content-register">
                    <div class="bg-slider-login">
                        <div class="mar-title-login">
                            <h2>
                                Registrarse<small class="on-right">Digital-Books</small>
                            </h2>
                        </div>
                        <hr>
                        <div class="mar-form-login">
                            <form action="guardarregistros.php" method="post" onSubmit="return noCampoUser(this);" onKeyUp="calcLong('pass',this,8); calcLong('log',this, 8);
                            calcLong('clv',this, 8); calcLong('nomcomp',this, 25); calcLong('ap',this, 25); calcLong('am',this, 25);">
                                <div class="tam-cont-input">
                                    <div class="input-control text">                                        
                                        <input name="log" id="log" type="text" placeholder="Nombre de Usuario" value="<?php if (isset($_GET["nomuser"])){ echo $_GET["nomuser"];}?>"/>
                                        <button class="btn-clear"></button>
                                        <div class="pos-error-registros"> 
                                            <p><?php
                                                    include("config.php"); /*Archivos de configuración de la bases de datos*/
                                                    if (isset($_GET["userexis"])){
                                                        if($_GET["userexis"]=="si"){
                                                            echo "El nombre de usuario ya existe";
                                                        }
                                                        /*header("refresh:2; url=index.php");*/
                                                    }
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="input-control password">
                                        <input name="pass" id="pass" type="password" value="<?php if (isset($_GET["pass"])){ echo $_GET["pass"];}?>" placeholder="Contraseña"/><!-- onkeypress="return validarNumLetras(event);" -->
                                        <button class="btn-reveal"></button>
                                    </div>
                                    <div class="input-control password">
                                        <input name="pass1" id="pass1" type="password" value="<?php if (isset($_GET["pass1"])){ echo $_GET["pass1"];}?>" placeholder="Repetir Contraseña"/> <!-- onkeypress="return validarNumLetras(event);"-->
                                        <button class="btn-reveal"></button>
                                        <div class="pos-error-registros"> 
                                            <p><?php if (isset($_GET["passiguales"])){
                                                    if($_GET["passiguales"]=="no"){
                                                        echo "Las contraseñas no coinciden.";   
                                                    }
                                                    /*header("refresh:2; url=index.php");*/
                                                }?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="input-control text">
                                        <input name="nomcomp" id="nomcomp" type="text" placeholder="Nombre o Nombres"
                                        value="<?php if (isset($_GET["nomcomp"])){ echo $_GET["nomcomp"];}?>"/>
                                        <button class="btn-clear"></button>
                                        <div class="pos-error-registros"> 
                                            <p><?php if (isset($_GET["nomcomp"])){
                                                    
                                                    /*header("refresh:2; url=index.php");*/
                                                }?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="input-control text">
                                        <input name="ap" id="ap" type="text" placeholder="Apellido Paterno"
                                        value="<?php if (isset($_GET["ap"])){ echo $_GET["ap"];}?>"/>
                                        <button class="btn-clear"></button>
                                        <div class="pos-error-registros"> 
                                            <p><?php if (isset($_GET["ap"])){
                                                    
                                                    /*header("refresh:2; url=index.php");*/
                                                }?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="input-control text">
                                        <input name="am" id="am" type="text" placeholder="Apellido Materno"
                                        value="<?php if (isset($_GET["am"])){ echo $_GET["am"];}?>"/>
                                        <button class="btn-clear"></button>
                                        <div class="pos-error-registros"> 
                                            <p><?php if (isset($_GET["am"])){
                                                    
                                                    /*header("refresh:2; url=index.php");*/
                                                }?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="input-control text"> <!-- onkeypress="return validarNumLetras(event);"-->
                                        <input name="clv" id="clv" type="text" placeholder="Clave de administrador" 
                                        value="<?php if (isset($_GET["clv"])){ echo $_GET["clv"];}?>"/>
                                        <button class="btn-clear"></button>
                                        <div class="pos-error-registros"> 
                                            <p><?php if (isset($_GET["clave"])){
                                                    if($_GET["clave"]=="no"){
                                                        echo "La clave no es correcta.";   
                                                    }
                                                }
                                                if (isset($_GET["claveunica"])){
                                                    if($_GET["claveunica"]=="no"){
                                                        echo "La clave no es correcta.";
                                                    }
                                                }
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="pos-can-registro">
                                    <a href="index.php" class="button link bg-pass"><p>Cancelar</p></a>
                                </div>
                                <div class="alig-bot-registro">
                                    <input type="submit" name="Submit" value="Registrarse" class="button bg-pass button large primary">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>