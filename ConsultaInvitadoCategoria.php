<?php
include("config.php"); /*Archivos de configuración de la bases de datos*/
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador</title>
        <link href="css/csslogin.css" rel="stylesheet">
        <LINK REL="Shortcut Icon" HREF="images/icono.png">
        <link href="css/metro-bootstrap.css" rel="stylesheet">
        <link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
        <link href="js/prettify/prettify.css" rel="stylesheet">

        <!-- Load JavaScript Libraries -->
        <script src="js/jquery/jquery.min.js"></script>
        <script src="js/jquery/jquery.widget.min.js"></script>
        <script src="js/jquery/jquery.mousewheel.js"></script>
        <script src="js/prettify/prettify.js"></script>
        <script src="js/metro.min.js"></script>

    </head>
    <body class="metro">
        <div id="content_page">
            <nav class="navigation-bar dark">
                <div class="navigation-bar-content">
                    <a href="admin.php" class="element"><span class="icon-grid-view"></span> Digitial Books <sup>1.0</sup></a>
                    <span class="element-divider"></span>
                    <a class="pull-menu" href="#"></a>
                    <ul class="element-menu">
                        <li>
                            <a class="dropdown-toggle" href="#">Acerca de</a>
                            <ul class="dropdown-menu dark" data-role="dropdown">
                                <li><a href="acerca.php">Acerca de</a></li>
                                <li><a href="programadores.php">Programadores</a></li>
                            </ul>
                        </li>
                    </ul>

                    <div class="no-tablet-portrait">
                        <span class="element-divider"></span>
                        <a class="element brand" href="admin.php"><span class="icon-spin"></span></a>
                        <span class="element-divider"></span>

                        <div class="element place-right">
                            <a class="dropdown-toggle" href="#">
                                <span class="icon-cog"></span>
                            </a>
                            <ul class="dropdown-menu place-right" data-role="dropdown" style="display: none;">
                                <li><a href="#">Cuenta</a></li>
                                <li><a href="#">Cambiar Nombre</a></li>
                                <li><a href="cerrarsesion.php">Salir</a></li>
                            </ul>
                        </div>
                        <span class="element-divider place-right"></span>
                        <button class="element image-button image-left place-right">
                            Invitado
                            <img src="images/me.jpg">
                        </button>
                    </div>
                </div>
            </nav>      
            <div class="row">
                <div class="column grid_3">
                    <ul class="dropdown-menu dark inverse open keep-open" style="position: relative; width: 200px; z-index: 1">
                        <li class="menu-title">Funciones</li>
                        <li><a href="#">Agregar Libros</a></li>
                        <li><a href="ConsultarInvitado.php">Buscar Libros</a></li>
                    </ul>
                </div>
                <div class="column grid_9">
                    
                    <?php

                        $conexion = mysql_connect(HOST, USERNAME,PASSWORD) or die("No se pudo conectar con el servidor");
                        mysql_select_db("digitalbooks",$conexion) or die ("Error en la conexión con la base de datos");    

                        if($_GET){
                            $id=$_GET["id"];
                            $result=mysql_query("select * from books where categoria='".$id."' and revisado=1 ",$conexion);
                            if ($row=mysql_num_rows($result)){

                    ?> 
                                <h3>Busqueda por: Categoria: <?php echo ("   ".$id); ?></h3> 
                                <table  border="1px" cellpadding="10" align="center">                
                            <?php
                                    while($fila=mysql_fetch_array($result)){
                                        $nombre_fichero = 'img/'.$fila['isbn'].'.jpg';

                                        if (file_exists($nombre_fichero)) {
                                            echo "<tr><td><img src=\"img/".$fila['isbn'].".jpg"."\" width=\"100px\" height=\"100px\"></td>";
                                        }else {
                                            echo "<tr><td><img src=\"img/pdficon.png"."\" width=\"50px\" height=\"50px\"></td>";
                                        }
                                        echo "<td><p>ISBN: ".$fila["isbn"]."</p>";
                                        echo "<p>Nombre: ".$fila["nombre"]."</p>";
                                        echo "<p>Autor: ".$fila["autor"]."</p>";
                                        echo "<p>Edicion: ".$fila["edicion"]."</p>";
                                        echo "<p>Editorial: ".$fila["editorial"]."</p>";
                                        echo "<p>Categoria: ".$fila["categoria"]."</p>";
                                        echo "<p><a href=\"files/".$fila['isbn'].".pdf"."\" class=\"linkli\">Leer</a><p>";
                            ?>
                                        </td></tr>
                                <?php 
                                    }      
                                ?> 
                                </table>                   
                    <?php
                            }else{
                                echo "<h5 >No se encontró ningun Libro con los datos porporcionados</h5>";
                            }
                            mysql_close($conexion);
                        }

                    ?>
                </div>    
            </div>
        </div>
    </body>
</html>