<?php
include("config.php"); /*Archivos de configuración de la bases de datos*/
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador</title>
        <link href="css/csslogin.css" rel="stylesheet">
        <LINK REL="Shortcut Icon" HREF="images/icono.png">
        <link href="css/metro-bootstrap.css" rel="stylesheet">
        <link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
        <link href="js/prettify/prettify.css" rel="stylesheet">

        <!-- Load JavaScript Libraries -->
        <script src="js/jquery/jquery.min.js"></script>
        <script src="js/jquery/jquery.widget.min.js"></script>
        <script src="js/jquery/jquery.mousewheel.js"></script>
        <script src="js/prettify/prettify.js"></script>
        <script src="js/metro.min.js"></script>

    </head>
    <body class="metro">
        <?php
        header("Content-Type: text/html;charset=utf-8");
        error_reporting(E_ALL ^ E_DEPRECATED);
        @session_start();
        if (!isset($_SESSION["usuario"])){
            session_unset();
            session_destroy();
            /*en caso de que la sesión sea incorrecta el mensaje de error va aquí*/
            header('Location: index.php?error=no');
        ?>
        <?php
        }else{
            /*en caso de que la sesión sea correcta*/
        ?>
        <div id="content_page">
            <nav class="navigation-bar dark">
                <div class="navigation-bar-content">
                    <a href="admin.php" class="element"><span class="icon-grid-view"></span> Digitial Books <sup>1.0</sup></a>
                    <span class="element-divider"></span>
                    <a class="pull-menu" href="#"></a>
                    <ul class="element-menu">
                        <li>
                            <a class="dropdown-toggle" href="#">Acerca de</a>
                            <ul class="dropdown-menu dark" data-role="dropdown">
                                <li><a href="acerca.php">Acerca de</a></li>
                                <li><a href="programadores.php">Programadores</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="no-tablet-portrait">
                        <span class="element-divider"></span>
                        <a class="element brand" href="#"><span class="icon-spin"></span></a>
                        <span class="element-divider"></span>
                        <div class="element place-right">
                            <a class="dropdown-toggle" href="#">
                                <span class="icon-cog"></span>
                            </a>
                            <ul class="dropdown-menu place-right" data-role="dropdown" style="display: none;">
                                <li><a href="#">Cuenta</a></li>
                                <li><a href="#">Cambiar Nombre</a></li>
                                <li><a href="cerrarsesion.php">Salir</a></li>
                            </ul>
                        </div>
                        <span class="element-divider place-right"></span>
                        <button class="element image-button image-left place-right">
                            <?php echo $_SESSION["nomuser"];?>
                            <img src="images/me.jpg">
                        </button>
                    </div>
                </div>
            </nav>      
            <div class="row">
                <div class="column grid_3">
                    <ul class="dropdown-menu dark inverse open keep-open" style="position: relative; width: 200px; z-index: 1">
                        <li class="menu-title">Funciones</li>
                        <li><a href="bookAlta.php">Agregar Libros</a></li>
                        <li><a href="eliminar.php">Eliminar Libros</a></li>
                        <li><a href="Consultar.php">Buscar Libros</a></li>
                    </ul>
                </div>

                <div class="column grid_9">
                    <div class="alig-lib-rev">
                        <div class="balloon right">
                        <?php
                        $conexion = mysql_connect(HOST, USERNAME,PASSWORD) or die("No se pudo conectar con el servidor");
                        mysql_select_db("digitalbooks",$conexion) or die ("Error en la conexión con la base de datos");    

                        if($_POST){
                            $opcion=$_POST["consulta"];
                            if($opcion=="Libre"){
                                $nombres=$_POST["text"];
                                $result=mysql_query("select * from books where (nombre like'%".$nombres."%' or autor like'%".$nombres."%' or isbn like'%".$nombres."%') and revisado=1",$conexion);
                            }elseif ($opcion=="isbn") {
                                $nombres=$_POST["text"];
                                $result=mysql_query("select * from books where isbn like'%".$nombres."%' and revisado=1",$conexion);
                            }elseif ($opcion=="nombre") {
                                $nombres=$_POST["text"];
                                $result=mysql_query("select * from books where nombre like'%".$nombres."%' and revisado=1",$conexion);
                            }elseif ($opcion=="autor") {
                                $nombres=$_POST["text"];
                                $result=mysql_query("select * from books where autor like'%".$nombres."%' and revisado=1",$conexion);
                            }
                        }
                        if ($row=mysql_num_rows($result)){

                    ?>
                            <div class="tab-control padding20" data-role="tab-control">
                                <ul class="tabs">
                                    <li class="active   "><a href="#write">Busqueda por: <?php echo ($opcion); echo ("   ".$nombres); ?></a></li>
                                </ul>
                                <div class="frames">
                                    <div id="write" class="frame" style="display: none;">
                            <table  border="1px" cellpadding="10" align="center">                
                            <?php
                                while($fila=mysql_fetch_array($result)){

                                    $nombre_fichero = 'img/'.$fila['isbn'].'.jpg';

                                    if (file_exists($nombre_fichero)) {
                                        echo "<tr><td><img src=\"img/".$fila['isbn'].".jpg"."\" width=\"100px\" height=\"100px\"></td>";
                                    }else {
                                        echo "<tr><td><img src=\"img/pdficon.png"."\" width=\"50px\" height=\"50px\"></td>";
                                    }
                                    echo "<td><p>ISBN: ".$fila["isbn"]."</p>";
                                    echo "<p>Nombre: ".$fila["nombre"]."</p>";
                                    echo "<p>Autor: ".$fila["autor"]."</p>";
                                    echo "<p>Edicion: ".$fila["edicion"]."</p>";
                                    echo "<p>Editorial: ".$fila["editorial"]."</p>";
                                    echo "<p>Categoria: ".$fila["categoria"]."</p>";
                            ?>
                                    <p><a href="modificar.php?id=<?php echo $fila["isbn"];?>">Modificar Datos</a></p>
                                    <p><a href="delete.php?id=<?php echo $fila["isbn"];?>">Eliminar Libro</a></p>
                            <?php
                                    echo "<p><a href=\"files/".$fila['isbn'].".pdf"."\" class=\"linkli\">Leer</a><p>";
                            ?>
                                    </td></tr>
                            <?php 
                                }      
                            ?> 
                            </table>                   
                    <?php
                        }else{
                            echo "<h5 >No se encontró ningun Libro con los datos porporcionados</h5>";
                        }
                        mysql_close($conexion);
                    ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>    
            </div>
        </div>
        <?php
        }/*se cierra la condición en caso de que la sesión sí se haya realizado correctamente*/
        ?>
    </body>
</html>