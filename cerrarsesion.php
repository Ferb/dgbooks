<?php
	include("config.php"); /*Archivos de configuración de la bases de datos*/
	header("Content-Type: text/html;charset=utf-8");
    //error_reporting(E_ALL ^ E_DEPRECATED);
    @session_start();
    if (!isset($_SESSION["usuario"])){
		session_unset();
		session_destroy();
		header('Location: index.php');
	}else{
		header('Location: index.php');
		echo ("Solo los usuarios que iniciaron sesion pueden cerrarla<br>");
		echo ("<a href = \"index.php\"> Loguearse </a> <br>");
	}
?>