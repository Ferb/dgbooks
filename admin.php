<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador</title>
        <link href="css/csslogin.css" rel="stylesheet">
        <LINK REL="Shortcut Icon" HREF="images/icono.png">
        <link href="css/metro-bootstrap.css" rel="stylesheet">
        <link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
        <link href="js/prettify/prettify.css" rel="stylesheet">

        <!-- Load JavaScript Libraries -->
        <script src="js/jquery/jquery.min.js"></script>
        <script src="js/jquery/jquery.widget.min.js"></script>
        <script src="js/jquery/jquery.mousewheel.js"></script>
        <script src="js/prettify/prettify.js"></script>
        <script src="js/metro.min.js"></script>

    </head>
    <body class="metro">
        <?php
        include("config.php"); /*Archivos de configuración de la bases de datos*/
        header("Content-Type: text/html;charset=utf-8");
        error_reporting(E_ALL ^ E_DEPRECATED);
        @session_start();
        if (!isset($_SESSION["usuario"])){
            session_unset();
            session_destroy();
            /*en caso de que la sesión sea incorrecta el mensaje de error va aquí*/
            header('Location: index.php?error=no');
        ?>
        <?php
        }else{
            /*en caso de que la sesión sea correcta*/
        ?>
        <div id="content_page">
            <nav class="navigation-bar dark">
                <div class="navigation-bar-content">
                    <a href="admin.php" class="element"><span class="icon-grid-view"></span> Digitial Books <sup>1.0</sup></a>
                    <span class="element-divider"></span>
                    <a class="pull-menu" href="#"></a>
                    <ul class="element-menu">
                        <li>
                            <a class="dropdown-toggle" href="#">Acerca de</a>
                            <ul class="dropdown-menu dark" data-role="dropdown">
                                <li><a href="acerca.php">Acerca de</a></li>
                                <li><a href="programadores.php">Programadores</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="no-tablet-portrait">
                        <span class="element-divider"></span>
                        <a class="element brand" href="admin.php"><span class="icon-spin"></span></a>
                        <span class="element-divider"></span>
                        <div class="element place-right">
                            <a class="dropdown-toggle" href="#">
                                <span class="icon-cog"></span>
                            </a>
                            <ul class="dropdown-menu place-right" data-role="dropdown" style="display: none;">
                                <li><a href="#">Cuenta</a></li>
                                <li><a href="#">Cambiar Nombre</a></li>
                                <li><a href="cerrarsesion.php">Salir</a></li>
                            </ul>
                        </div>
                        <span class="element-divider place-right"></span>
                        <button class="element image-button image-left place-right">
                            <?php echo $_SESSION["nomuser"];?>
                            <img src="images/me.jpg">
                        </button>
                    </div>
                </div>
            </nav>  
            <div class="row">
                <div class="column grid_3">
                    <ul class="dropdown-menu dark inverse open keep-open" style="position: relative; width: 200px; z-index: 1">
                        <li class="menu-title">Funciones</li>
                        <li><a href="bookAlta.php">Agregar Libros</a></li>
                        <li><a href="eliminar.php">Eliminar Libros</a></li>
                        <li><a href="Consultar.php">Buscar Libros</a></li>
                    </ul>
                </div>
                <div class="column grid_9">
                    <div class="alig-lib-rev">
                        <div class="balloon right">
                            <div class="tab-control padding20" data-role="tab-control">
                                <ul class="tabs">
                                    <li class="active   "><a href="#write">Libros sin revisar</a></li>
                                    <li class=""><a href="#preview">Libros revisados</a></li>
                                </ul>
                                <div class="frames">
                                    <?php 
                                        if (isset($_GET["revisar"])){
                                            if ($_GET["revisar"]=="si"){
                                                echo "<div class=\"men-act-revision\">
                                                        <div class=\"balloon bottom men-act-revision color-revi-act\">
                                                            El libro con isbn \"".$_GET["isbn"]."\" ha sido agregado al sistema.
                                                        </div>
                                                    </div>";
                                            }else{
                                                echo "<div class=\"men-act-revision visibility=\"hidden\"\">
                                                        <div class=\"balloon bottom men-act-revision\"></div>
                                                    </div>";    
                                            }
                                        }
                                    ?>

                                    <div id="write" class="frame" style="display: none;">
                                        <?php 
                                            $conexion = mysql_connect(HOST, USERNAME, PASSWORD);
                                            mysql_select_db("digitalbooks", $conexion);
                                            $query1 = "SELECT count(*) from books where revisado=0";
                                            $cont = mysql_query($query1, $conexion) or die(mysql_error());
                                            while ($row = mysql_fetch_assoc($cont)) {
                                                $total = $row['count(*)'];
                                            }
                                            if($total!=0){
                                                $query = "SELECT isbn, nombre, autor,edicion from books where revisado=0";
                                                $res = mysql_query($query, $conexion) or die(mysql_error());
                                                echo "<table class=\"table hovered\">
                                                        <thead>
                                                            <tr><th class=\"text-left\">Imágenes</th><th class=\"text-left\">Título</th><th class=\"text-left\">Autor</th>
                                                            <th class=\"text-left\">ISBN</th></tr></thead><tbody>";
                                                while ($row = mysql_fetch_assoc($res)) {
                                                    $nombre_fichero = false;
                                                    $nombre_fichero1 = "imagetemp/".$row['isbn'].".jpg";
                                                    $nombre_fichero2 = "imagetemp/".$row['isbn'].".png";
                                                    $nombre_fichero3 = "imagetemp/".$row['isbn'].".gif";
                                                    $nombre_fichero4 = "imagetemp/".$row['isbn'].".svg";
                                                    if (file_exists($nombre_fichero1)){
                                                        $nombre_fichero = true;
                                                        $image = $row['isbn'].".jpg";
                                                    }else{
                                                        if (file_exists($nombre_fichero2)){
                                                            $nombre_fichero = true;
                                                            $image = $row['isbn'].".png";
                                                        }else{
                                                            if (file_exists($nombre_fichero3)){
                                                                $nombre_fichero = true;
                                                                $image = $row['isbn'].".gif";
                                                            }else{
                                                                if (file_exists($nombre_fichero4)){
                                                                    $nombre_fichero = true;
                                                                    $image = $row['isbn'].".svg";
                                                                }else{
                                                                    $nombre_fichero = false;
                                                                }       
                                                            }       
                                                        }
                                                    }
                                                    if ($nombre_fichero){
                                                        echo "
                                                            <tr>
                                                                <td>
                                                                    <img src=\"imagetemp/".$image."\" width=\"80px\" height=\"80px\"> 
                                                                    <div class=\"transparent flotar-revis\">
                                                                        <a href=\"filestemp/".$row['isbn'].".pdf\" target=\"_new\"><button class=\"info\"><i class=\"icon-file-pdf on-left\"></i>Ver</button></a>
                                                                        <a href=\"actuarevisar.php?isbn=".$row['isbn']."&img=".$image."\" ><button class=\"success\"><i class=\"icon-floppy on-left\"></i>Marcar como revisado</button></a>
                                                                    </div>
                                                                </td>
                                                                <td width=\"35%\">".$row['nombre']."</td>
                                                                <td class=\"right\">".$row['autor']."</td>
                                                                <td class=\"right\">".$row['isbn']."</td>
                                                            </tr>
                                                        ";
                                                    }else{
                                                        echo "
                                                            <tr>
                                                                <td>
                                                                    <img src=\"images/pdficon.png\" width=\"80px\" height=\"80px\"> 
                                                                    <div class=\"transparent flotar-revis\">
                                                                        <a href=\"filestemp/".$row['isbn'].".pdf\" target=\"_new\"><button class=\"info\"><i class=\"icon-file-pdf on-left\"></i>Ver</button></a>
                                                                        <a href=\"actuarevisar.php?isbn=".$row['isbn']."&img=no\" ><button class=\"success\"><i class=\"icon-floppy on-left\"></i>Marcar como revisado</button></a>
                                                                    </div>
                                                                </td>
                                                                <td width=\"35%\">".$row['nombre']."</td>
                                                                <td class=\"right\">".$row['autor']."</td>
                                                                <td class=\"right\">".$row['isbn']."</td>
                                                            </tr>
                                                        ";
                                                    }
                                                }
                                                echo "</tbody><tfoot></tfoot></table>";
                                            }else{
                                                echo "No hay libros por revisar";
                                            }
                                            mysql_close($conexion);
                                        ?>
                                    </div>
                                    <div id="preview" class="frame" style="display: block;">
                                        <?php 
                                            $conexion = mysql_connect(HOST, USERNAME,PASSWORD) or die("No se pudo conectar con el servidor");
                                            mysql_select_db("digitalbooks", $conexion);
                                            $query1 = "SELECT count(*) from books where revisado=1";
                                            $cont = mysql_query($query1, $conexion) or die(mysql_error());
                                            while ($row = mysql_fetch_assoc($cont)) {
                                                $total = $row['count(*)'];
                                            }
                                            if($total!=0){
                                                $query = "SELECT isbn, nombre, autor,edicion from books where revisado=1";
                                                $res = mysql_query($query, $conexion) or die(mysql_error());
                                                echo "<table class=\"table hovered\">
                                                        <thead>
                                                            <tr><th class=\"text-left\">Imágenes</th><th class=\"text-left\">Título</th><th class=\"text-left\">Autor</th>
                                                            <th class=\"text-left\">ISBN</th></tr></thead><tbody>";
                                                while ($row = mysql_fetch_assoc($res)) {
                                                    $nombre_fichero = false;
                                                    $nombre_fichero1 = "img/".$row['isbn'].".jpg";
                                                    $nombre_fichero2 = "img/".$row['isbn'].".png";
                                                    $nombre_fichero3 = "img/".$row['isbn'].".gif";
                                                    $nombre_fichero4 = "img/".$row['isbn'].".svg";
                                                    if (file_exists($nombre_fichero1)){
                                                        $nombre_fichero = true;
                                                        $image = $row['isbn'].".jpg";
                                                    }else{
                                                        if (file_exists($nombre_fichero2)){
                                                            $nombre_fichero = true;
                                                            $image = $row['isbn'].".png";
                                                        }else{
                                                            if (file_exists($nombre_fichero3)){
                                                                $nombre_fichero = true;
                                                                $image = $row['isbn'].".gif";
                                                            }else{
                                                                if (file_exists($nombre_fichero4)){
                                                                    $nombre_fichero = true;
                                                                    $image = $row['isbn'].".svg";
                                                                }else{
                                                                    $nombre_fichero = false;
                                                                }       
                                                            }       
                                                        }
                                                    }
                                                    if ($nombre_fichero){
                                                        echo "
                                                            <tr>
                                                                <td>
                                                                    <img src=\"img/".$image."\" width=\"80px\" height=\"80px\"> 
                                                                    <div class=\"transparent flotar-revis\">
                                                                        <a href=\"files/".$row['isbn'].".pdf\" target=\"_new\"><button class=\"info\"><i class=\"icon-file-pdf on-left\"></i>Ver</button></a>
                                                                    </div>
                                                                </td>
                                                                <td width=\"35%\">".$row['nombre']."</td>
                                                                <td class=\"right\">".$row['autor']."</td>
                                                                <td class=\"right\">".$row['isbn']."</td>
                                                            </tr>
                                                        ";
                                                    }else{
                                                        echo "
                                                            <tr>
                                                                <td>
                                                                    <img src=\"img/pdficon.png\" width=\"80px\" height=\"80px\"> 
                                                                    <div class=\"transparent flotar-revis\">
                                                                        <a href=\"files/".$row['isbn'].".pdf\" target=\"_new\"><button class=\"info\"><i class=\"icon-file-pdf on-left\"></i>Ver</button></a>
                                                                    </div>
                                                                </td>
                                                                <td width=\"35%\">".$row['nombre']."</td>
                                                                <td class=\"right\">".$row['autor']."</td>
                                                                <td class=\"right\">".$row['isbn']."</td>
                                                            </tr>
                                                        ";
                                                    }
                                                }
                                                echo "</tbody><tfoot></tfoot></table>";
                                            }
                                            mysql_close($conexion);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        <?php
        }/*se cierra la condición en caso de que la sesión sí se haya realizado correctamente*/
        ?>
    </body>
</html>
