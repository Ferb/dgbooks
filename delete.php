<?php
include("config.php"); /*Archivos de configuraci�n de la bases de datos*/
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar</title>
        <link href="css/csslogin.css" rel="stylesheet">
        <LINK REL="Shortcut Icon" HREF="images/icono.png">
        <link href="css/metro-bootstrap.css" rel="stylesheet">
        <link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
        <link href="js/prettify/prettify.css" rel="stylesheet">

        <!-- Load JavaScript Libraries -->
        <script src="js/jquery/jquery.min.js"></script>
        <script src="js/jquery/jquery.widget.min.js"></script>
        <script src="js/jquery/jquery.mousewheel.js"></script>
        <script src="js/prettify/prettify.js"></script>
        <script src="js/metro.min.js"></script>

    </head>
    <body class="metro">
        <?php
        header("Content-Type: text/html;charset=utf-8");
        error_reporting(E_ALL ^ E_DEPRECATED);
        @session_start();
        if (!isset($_SESSION["usuario"])){
            session_unset();
            session_destroy();
            /*en caso de que la sesi�n sea incorrecta el mensaje de error va aqu�*/
            header('Location: index.php?error=no');
        ?>
        <?php
        }else{
            /*en caso de que la sesi�n sea correcta*/
        ?>
        <div id="content_page">
            <nav class="navigation-bar dark">
                <div class="navigation-bar-content">
                    <a href="admin.php" class="element"><span class="icon-grid-view"></span> Digitial Books <sup>1.0</sup></a>
                    <span class="element-divider"></span>
                    <a class="pull-menu" href="#"></a>
                    <ul class="element-menu">
                        <li>
                            <a class="dropdown-toggle" href="#">Acerca de</a>
                            <ul class="dropdown-menu dark" data-role="dropdown">
                                <li><a href="acerca.php">Acerca de</a></li>
                                <li><a href="programadores.php">Programadores</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="no-tablet-portrait">
                        <span class="element-divider"></span>
                        <a class="element brand" href="eliminar.php"><span class="icon-spin"></span></a>
                        <span class="element-divider"></span>
                        <div class="element place-right">
                            <a class="dropdown-toggle" href="#">
                                <span class="icon-cog"></span>
                            </a>
                            <ul class="dropdown-menu place-right" data-role="dropdown" style="display: none;">
                                <li><a href="#">Cuenta</a></li>
                                <li><a href="#">Cambiar Nombre</a></li>
                                <li><a href="cerrarsesion.php">Salir</a></li>
                            </ul>
                        </div>
                        <span class="element-divider place-right"></span>
                        <button class="element image-button image-left place-right">
                            <?php echo $_SESSION["nomuser"];?>
                            <img src="images/me.jpg">
                        </button>
                    </div>
                </div>
            </nav>      
            <div class="row">
                <div class="column grid_3">
                    <ul class="dropdown-menu dark inverse open keep-open" style="position: relative; width: 200px; z-index: 1">
                        <li class="menu-title">Funciones</li>
                        <li><a href="bookAlta.php">Agregar Libros</a></li>
                        <li><a href="eliminar.php">Eliminar Libros</a></li>
                        <li><a href="Consultar.php">Buscar Libros</a></li>
                    </ul>
                </div>
                <div class="column grid_9">
                    <div class="alig-lib-rev">
                        <div class="balloon right">
                            <div class="tab-control padding20" data-role="tab-control">
                                <ul class="tabs">
                                    <li class="active   "><a href="#write">Eliminando Libros</a></li>
                                </ul>
                                <div class="frames">
                                    <div id="write" class="frame" style="display: none;">
                                        <p align="center">
            <?php
                if($_GET) {
                    $isbn=$_GET["id"];
                    
                    $conexion = mysql_connect(HOST, USERNAME,PASSWORD) or die("No se pudo conectar con el servidor");
                    mysql_select_db("digitalbooks", $conexion) or die("No se pudo conectar con la base de datos");
                    
                    $result=mysql_query("DELETE FROM books WHERE isbn='".$isbn."'", $conexion);
                            if($result) {

                                $destino2 =  "img/".$isbn.".jpg";
                                if (file_exists($destino2)) {
                                    unlink($destino2);
                                }
                                $destino2 =  "files/".$isbn.".pdf";
                                unlink($destino2);

                                echo "REGISTRO ELIMINADO!";
                            } else {
                                $numErr=mysql_errno($conexion);
                                $descErr=mysql_error($conexion);
                                echo "No se pudo eliminar el registro<br />";
                                echo "N� de error: ".$numErr." * Descipci�n: ".$decErr;
                            }
                    mysql_close($conexion);
                }
                ?>
                </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }/*se cierra la condici�n en caso de que la sesi�n s� se haya realizado correctamente*/
        ?>
    </body>
</html>