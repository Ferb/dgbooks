<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador</title>
        <link href="css/csslogin.css" rel="stylesheet">
        <LINK REL="Shortcut Icon" HREF="images/icono.png">
        <link href="css/metro-bootstrap.css" rel="stylesheet">
        <link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
        <link href="js/prettify/prettify.css" rel="stylesheet">

        <!-- Load JavaScript Libraries -->
        <script src="js/jquery/jquery.min.js"></script>
        <script src="js/jquery/jquery.widget.min.js"></script>
        <script src="js/jquery/jquery.mousewheel.js"></script>
        <script src="js/prettify/prettify.js"></script>
        <script src="js/metro.min.js"></script>

    </head>
    <body class="metro">
        <?php
        include("config.php"); /*Archivos de configuración de la bases de datos*/
        header("Content-Type: text/html;charset=utf-8");
        error_reporting(E_ALL ^ E_DEPRECATED);
        @session_start();
        if (!isset($_SESSION["usuario"])){
            session_unset();
            session_destroy();
            /*en caso de que la sesión sea incorrecta el mensaje de error va aquí*/
            header('Location: index.php?error=no');
        ?>
        <?php
        }else{
            /*en caso de que la sesión sea correcta*/
        ?>
        <div id="content_page">
            <nav class="navigation-bar dark">
                <div class="navigation-bar-content">
                    <a href="admin.php" class="element"><span class="icon-grid-view"></span> Digitial Books <sup>1.0</sup></a>
                    <span class="element-divider"></span>
                    <a class="pull-menu" href="#"></a>
                    <ul class="element-menu">
                        <li>
                            <a class="dropdown-toggle" href="#">Acerca de</a>
                            <ul class="dropdown-menu dark" data-role="dropdown">
                                <li><a href="acerca.php">Digital Books</a></li>
                                <li><a href="programadores.php">Programador</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="no-tablet-portrait">
                        <span class="element-divider"></span>
                        <a class="element brand" href="admin.php"><span class="icon-spin"></span></a>
                        <span class="element-divider"></span>
                        <div class="element place-right">
                            <a class="dropdown-toggle" href="#">
                                <span class="icon-cog"></span>
                            </a>
                            <ul class="dropdown-menu place-right" data-role="dropdown" style="display: none;">
                                <li><a href="#">Mi cuenta</a></li>
                                <!--<li><a href="#">Cambiar Nombre</a></li>-->
                                <li><a href="cerrarsesion.php">Salir</a></li>
                            </ul>
                        </div>
                        <span class="element-divider place-right"></span>
                        <button class="element image-button image-left place-right">
                            <?php echo $_SESSION["nomuser"];?>
                            <img src="images/me.jpg">
                        </button>
                    </div>
                </div>
            </nav>  
            <div class="row">
                <div class="column grid_3">
                    <ul class="dropdown-menu dark inverse open keep-open" style="position: relative; width: 200px; z-index: 1">
                        <li class="menu-title">Funciones</li>
                        <li><a href="bookAlta.php">Agregar Libros</a></li>
                        <li><a href="eliminar.php">Eliminar Libros</a></li>
                        <li><a href="Consultar.php">Buscar Libros</a></li>
                        <li><a href="admin.php">Descargar Libros</a></li>
                        <li><a href="videos.php">Videos</a></li>
                        <li><a href="imagenes.php">Imágenes</a></li>
                        <!--<li><a href="#">Publicaciones</a></li>-->
                    </ul>
                </div>
                <div class="column grid_9">
                    <div class="alig-lib-rev">
                        <div class="balloon right">
                            <div class="tab-control padding20" data-role="tab-control">
                                <ul class="tabs">
                                    <!--<li class="active"><a href="#write">Libros sin revisar</a></li>-->
                                    <li class="active"><a href="#preview">Videos</a></li>
                                </ul>
                                                                
                                <video id="sampleMovie" width="640" height="360" preload controls>
                                    <!--<source src="HTML5Sample_H264.mov" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
                                    <source src="HTML5Sample_Ogg.ogv" type='video/ogg; codecs="theora, vorbis"' />-->
                                    <source src="img/50anos.webm" type='video/webm; codecs="vp8, vorbis"' />
                                </video>                                

                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        <?php
        }/*se cierra la condición en caso de que la sesión sí se haya realizado correctamente*/
        ?>
    </body>
</html>
