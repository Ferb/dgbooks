<?php
include("config.php"); /*Archivos de configuración de la bases de datos*/
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador</title>
        <link href="css/csslogin.css" rel="stylesheet">
        <LINK REL="Shortcut Icon" HREF="images/icono.png">
        <link href="css/metro-bootstrap.css" rel="stylesheet">
        <link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
        <link href="js/prettify/prettify.css" rel="stylesheet">

        <!-- Load JavaScript Libraries -->
        <script src="js/jquery/jquery.min.js"></script>
        <script src="js/jquery/jquery.widget.min.js"></script>
        <script src="js/jquery/jquery.mousewheel.js"></script>
        <script src="js/prettify/prettify.js"></script>
        <script src="js/metro.min.js"></script>

    </head>
    <body class="metro">
        <?php
        header("Content-Type: text/html;charset=utf-8");
        error_reporting(E_ALL ^ E_DEPRECATED);
        @session_start();
        if (!isset($_SESSION["usuario"])){
            session_unset();
            session_destroy();
            /*en caso de que la sesión sea incorrecta el mensaje de error va aquí*/
            header('Location: index.php?error=no');
        ?>
        <?php
        }else{
            /*en caso de que la sesión sea correcta*/
        ?>
        <div id="content_page">
            <nav class="navigation-bar dark">
                <div class="navigation-bar-content">
                    <a href="admin.php" class="element"><span class="icon-grid-view"></span> Digitial Books <sup>1.0</sup></a>
                    <span class="element-divider"></span>
                    <a class="pull-menu" href="#"></a>
                    <ul class="element-menu">
                        <li>
                            <a class="dropdown-toggle" href="#">Acerca de</a>
                            <ul class="dropdown-menu dark" data-role="dropdown">
                                <li><a href="acerca.php">Acerca de</a></li>
                                <li><a href="programadores.php">Programadores</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="no-tablet-portrait">
                        <span class="element-divider"></span>
                        <a class="element brand" href="admin.php"><span class="icon-spin"></span></a>
                        <span class="element-divider"></span>
                        <div class="element place-right">
                            <a class="dropdown-toggle" href="#">
                                <span class="icon-cog"></span>
                            </a>
                            <ul class="dropdown-menu place-right" data-role="dropdown" style="display: none;">
                                <li><a href="#">Cuenta</a></li>
                                <li><a href="#">Cambiar Nombre</a></li>
                                <li><a href="cerrarsesion.php">Salir</a></li>
                            </ul>
                        </div>
                        <span class="element-divider place-right"></span>
                        <button class="element image-button image-left place-right">
                            <?php echo $_SESSION["nomuser"];?>
                            <img src="images/me.jpg">
                        </button>
                    </div>
                </div>
            </nav>      
            <div class="row">
                <div class="column grid_3">
                    <ul class="dropdown-menu dark inverse open keep-open" style="position: relative; width: 200px; z-index: 1">
                        <li class="menu-title">Funciones</li>
                        <li><a href="bookAlta.php">Agregar Libros</a></li>
                        <li><a href="eliminar.php">Eliminar Libros</a></li>
                        <li><a href="Consultar.php">Buscar Libros</a></li>
                    </ul>
                </div>

                <div class="column grid_9">
                    <div class="alig-lib-rev">
                        <div class="balloon right">
                            <div class="tab-control padding20" data-role="tab-control">
                                <ul class="tabs">
                                    <li class="active   "><a href="#write"><h3>Búsquedas por:</h3></a></li>
                                </ul>
                                <div class="frames">
                                    <div id="write" class="frame" style="display: none;">
                                        <?php
                        $long=0;
                        $conexion = mysql_connect(HOST, USERNAME,PASSWORD) or die("No se pudo conectar con el servidor");
                        mysql_select_db("digitalbooks",$conexion) or die ("Error en la conexión con la base de datos");    
                        $result=mysql_query("select distinct categoria from books",$conexion);

                        $long=mysql_num_rows($result);
                        $i=0;
                        if ($long=mysql_num_rows($result)){
                            $i=0;
                            while($fila=mysql_fetch_array($result)){
                                $cadena[$i]=$fila["categoria"];
                                $i++;
                            }
                        }
                        mysql_close($conexion);
                    ?>
                    <form  method="post" enctype="multipart/form-data" action="ConsultaPrevia.php" name="Submit">
                        <div>
                            <div class="">
                                <div class="input-control radio margin10" data-role="input-control">
                                    <label>
                                        <input type="radio" name="consulta" checked="true" value="Libre">
                                        <span class="check"></span>
                                        Libre
                                    </label>
                                </div>
                                <div class="input-control radio margin10" data-role="input-control" >
                                    <label>
                                        <input type="radio"  name="consulta" value="nombre">
                                        <span class="check"></span>
                                        Nombre
                                    </label>
                                </div>
                                <div class="input-control radio margin10" data-role="input-control">
                                    <label>
                                        <input type="radio" name="consulta" value="isbn">
                                        <span class="check"></span>
                                        ISBN
                                    </label>
                                </div>
                                <div class="input-control radio margin10" data-role="input-control">
                                    <label>
                                        <input type="radio"  name="consulta" value="autor">
                                        <span class="check"></span>
                                        Autor
                                   </label>
                                </div>
                                <div class="input-control text margin1">
                                    <input type="text" name="text" />
                                    <button class="btn-search" type="submit" name="Submit"> </button>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="accordion" data-role="accordion" >
                                    <div class="accordion-frame" style="float: left;">
                                        <a href="#" class="heading">
                                             <label> 
                                                Categoria
                                            </label>
                                        </a>
                                        <div class="content sidebar light" style="padding: 0px;">
                                            <ul>
                                                <?php  
                                                    $i=0;
                                                    while($i < $long){
                                                ?>
                                                        <li style="padding: 1px;"><a href="categoria.php?id=<?php echo $cadena[$i];?>"><?php echo $cadena[$i]; ?></a></li>
                                                <?php
                                                        $i++;
                                                    }  
                                                ?>
                                            </ul>
                                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form> 
                    <br><br><br><br><br><br>
                                    </div>
                                    <div id="preview" class="frame" style="display: block;">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>    
            </div>
        </div>
        <?php
        }/*se cierra la condición en caso de que la sesión sí se haya realizado correctamente*/
        ?>
    </body>
</html>