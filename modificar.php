<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Modificar</title>
    <link href="css/csslogin.css" rel="stylesheet">
    <LINK REL="Shortcut Icon" HREF="images/icono.png">
    <link href="css/metro-bootstrap.css" rel="stylesheet">
    <link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
    <link href="js/prettify/prettify.css" rel="stylesheet">

    <!-- Load JavaScript Libraries -->
    <script src="js/jquery/jquery.min.js"></script>
    <script src="js/jquery/jquery.widget.min.js"></script>
    <script src="js/jquery/jquery.mousewheel.js"></script>
    <script src="js/prettify/prettify.js"></script>
    <script src="js/metro.min.js"></script>

  </head>
  <body class="metro">
    <?php
    include("config.php"); /*Archivos de configuración de la bases de datos*/
    header("Content-Type: text/html;charset=utf-8");
    error_reporting(E_ALL ^ E_DEPRECATED);
    @session_start();
    if (!isset($_SESSION["usuario"])){
      session_unset();
      session_destroy();
      /*en caso de que la sesión sea incorrecta el mensaje de error va aquí*/
      header('Location: index.php?error=no');
    ?>
    <?php
    }else{
      /*en caso de que la sesión sea correcta*/
    ?>
    <div id="content_page">
      <nav class="navigation-bar dark">
        <div class="navigation-bar-content">
          <a href="admin.php" class="element"><span class="icon-grid-view"></span> Digitial Books <sup>1.0</sup></a>
          <span class="element-divider"></span>
          <a class="pull-menu" href="#"></a>
          <ul class="element-menu">
            <li>
              <a class="dropdown-toggle" href="#">Acerca de</a>
              <ul class="dropdown-menu dark" data-role="dropdown">
                <li><a href="acerca.php">Acerca de</a></li>
                <li><a href="programadores.php">Programadores</a></li>
              </ul>
            </li>
          </ul>
          <div class="no-tablet-portrait">
            <span class="element-divider"></span>
            <a class="element brand" href="Consultar.php"><span class="icon-spin"></span></a>
            <span class="element-divider"></span>
            <div class="element place-right">
              <a class="dropdown-toggle" href="#">
                <span class="icon-cog"></span>
              </a>
              <ul class="dropdown-menu place-right" data-role="dropdown" style="display: none;">
                <li><a href="#">Cuenta</a></li>
                <li><a href="#">Cambiar Nombre</a></li>
                <li><a href="cerrarsesion.php">Salir</a></li>
              </ul>
            </div>
            <span class="element-divider place-right"></span>
            <button class="element image-button image-left place-right">
              <?php echo $_SESSION["nomuser"];?>
              <img src="images/me.jpg">
            </button>
          </div>
        </div>
      </nav>      
      <div class="row">
        <div class="column grid_3">
          <ul class="dropdown-menu dark inverse open keep-open" style="position: relative; width: 200px; z-index: 1">
            <li class="menu-title">Funciones</li>
            <li><a href="bookAlta.php">Agregar Libros</a></li>
            <li><a href="eliminar.php">Eliminar Libros</a></li>
            <li><a href="Consultar.php">Buscar Libros</a></li>
          </ul>
        </div>
        <div class="column grid_9">
          <div class="alig-lib-rev">
            <div class="balloon right">
                <div class="tab-control padding20" data-role="tab-control">
                    <ul class="tabs">
                        <li class="active   "><a href="#write">Modificando Registros</a></li>
                    </ul>
                    <div class="frames">
                        <div id="write" class="frame" style="display: none;">
      <p align="center">
          <?php
        if($_GET) 
        {
          $id=$_GET["id"];
      
        $conexion = mysql_connect(HOST, USERNAME,PASSWORD) or die("No se pudo conectar con el servidor");
        mysql_select_db("digitalbooks", $conexion) or die("No se pudo conectar con la base de datos");
        $result=mysql_query("select * from books where isbn='".$id."'", $conexion);
      
        $fila=mysql_fetch_array($result);
       ?>
        
            <form name="form1" method="post" action="modificar2.php">
              <table width="200" border="2" align="center">
                <tr>
                  <td><table width="329" border="0" align="center">
                      <tr>
                        <td width="151"></td>
                        <td width="168"><label>
                          <input name="id" type="text" id="id" value=<?php echo ($fila["isbn"])?> readonly>
                        </label></td>
                      </tr>
                     <tr>
                        <td width="151">Nombre</td>
                        <td width="168"><label>
                          <textarea name="Nombre" id="Nombre"><?php echo ($fila["nombre"])?></textarea>
                        </label></td>
                      </tr>
                      <tr>
                        <td>Autor </td>
                        <td><label>
                          <textarea name="Autor" id="Autor"><?php echo ($fila["autor"])?></textarea>
                        </label></td>
                      </tr>
                      <tr>
                        <td>Edición </td>
                        <td><label>
                          <textarea name="Edicion" id="Edicion"><?php echo ($fila["edicion"])?></textarea>
                        </label></td>
                      </tr>
                      <tr>
                        <td>Editorial</td>
                        <td><label>
                          <textarea name="Editorial" id="Editorial"><?php echo ($fila["editorial"])?></textarea>
                        </label></td>
                      </tr>
                      <tr>
                        <td>Categoría</td>
                        <td><label>
                          <textarea name="Categoria" id="Categoria"><?php echo ($fila["categoria"])?></textarea>
                        </label></td>
                      </tr>
                      <tr>
                        <td>Revisado</td>
                        <td><label>
                          <textarea name="Revisado" id="Revisado"><?php echo ($fila["revisado"])?></textarea>
                        </label></td>
                      </tr>
                      <tr>
                        <td colspan="2"><div align="center">
                            <input type="submit" name="Submit" value="Enviar" />
                            <input type="reset" name="Submit2" value="Limpiar" />
                          </div>
                            <label></label>
                            <label></label></td>
                      </tr>
                  </table></td>
                </tr>
              </table>
            </form>
            <?php
              mysql_close($conexion);
            }
            ?>
              </p>          
                        </div>
                    </div>
                </div>
            </div>
        </div>
      

            </div>    
          </div>
        </div>
        <?php
        }/*se cierra la condición en caso de que la sesión sí se haya realizado correctamente*/
        ?>
  </body>
</html>