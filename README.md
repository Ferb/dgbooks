Bienvenidos al proyecto DGBooks

El proyecto consiste en crear un sitio utilizando PHP y MySQL para alojar archivos o libros digitales en formato PDF. También se pueden logear y crear usuarios de manera local. Para administrar el sitio de Web se utiliza PHPMyAdmin de Xampp 6.3. Adicionalmente se le han agreado algunas validaciones a los campos, tamaño de cadenas, tipo de caracteres aceptados entre otras.

Este un proyecto lo entregué con mi equipo como trabajo final del 7 semestre de la carrera en Informática, espero y les sea de utilidad. Más adelante iré haciendo correcciones o actualizaciones del mismo, ya que existen partes en las que aún no funcionan correctamente.

Debo mencionar que algunas librerias que utilicé y el entorno de la inerfaz gráfica, fue obtenido del proyecto https://github.com/olton/Metro-UI-CSS gracias al autor de este proyecto, se realizó el proyecto DGBooks de manera más rápida reutilizando los componentes de interfaz gráfica.

Uso: Para utilizar este proyecto primero es necesario instalar Xampp. Una vez instaladao crear una base datos llamada digitalbooks dentro de esta base datos ejecutar el script digitalbooks.sql que viene dentro del directorio de dgbooks y con eso se crea la base de datos. Par comenzar a usar el sitio web, registrarse con la clave 12345678, el primer usuario que se registre será un admistrador que administre todo el sitio y con eso se puede comenzar a utilizar el sitio completo. El archivo config.php contiene a descripción e información de la base de datos, tan solo modificar ese archivo se modifica en todo el sitio.

Más adelante iré actualizando más funcionalidades del proyecto DGBooks, por el momento aún no está completo.
