<?php 
    include("config.php"); /*Archivos de configuración de la bases de datos*/
    header("Content-Type: text/html;charset=utf-8");
    error_reporting(E_ALL ^ E_DEPRECATED);
    @session_start();
    if (!isset($_SESSION["usuario"])){
        session_unset();
        session_destroy();
        /*en caso de que la sesión sea incorrecta el mensaje de error va aquí*/
        header('Location: index.php?error=no');
    }
    else{
        $status = "";
        $status2 = "";

        if (isset($_POST["action"])){
            if ($_POST["action"] == "upload") {
            $vacioPDF=$_FILES["archivo"]['name'];
            $vacioImagen=$_FILES["imagen"]['name'];
        if ($vacioPDF!="" or $vacioImagen!="") {
            // obtenemos los datos del archivo 
            $tamano = $_FILES["archivo"]['size'];
            $tipo = $_FILES["archivo"]['type'];
            $archivo = $_FILES["archivo"]['name'];

            $tamano2 = $_FILES["imagen"]['size'];
            $tipo2 = $_FILES["imagen"]['type'];
            $archivo2 = $_FILES["imagen"]['name'];

            //$prefijo = substr(md5(uniqid(rand())),0,6); Genera un random y luego lo encripta para concatenarlo con el nombre del archivo
    
            /*Conexión a la base de datos*/
            $conexion = mysql_connect(HOST, USERNAME,PASSWORD) or die("No se pudo conectar con el servidor");
            mysql_select_db("digitalbooks", $conexion);
            $query = "insert into books values ('".$_POST["nombre"]."','".$_POST["autor"]."','".$_POST["edicion"]."','".$_POST["isbn"]."','".$_POST["editorial"]."','".$_POST["categoria"]."', 1)";
            mysql_query($query, $conexion) or die("<p>".mysql_error()."</p>");
            mysql_close($conexion);

            if ($archivo != "") {
                // guardamos el archivo a la carpeta files
                //$destino =  "files/".$prefijo."_".$archivo;
                //$destino =  "files/".$archivo;
                $destino =  "files/".$_POST["isbn"].".pdf";
                if (copy($_FILES['archivo']['tmp_name'],$destino)) {
                    $status = "Archivo subido: <b>".$archivo."</b>";
                } 
                else {
                    $status = "Error al subir el archivo";
                }
            } 
            else {
                $status = "Error al subir archivo";
            }

            if ($archivo2 != "") {
                // guardamos el archivo a la carpeta files
                //$destino =  "files/".$prefijo."_".$archivo;
                //$destino =  "files/".$archivo;
                $destino2 =  "img/".$_POST["isbn"].".jpg";
                if (copy($_FILES['imagen']['tmp_name'],$destino2)) {
                    $status2 = "Archivo subido: <b>".$archivo2."</b>";
                } 
                else {
                    $status2 = "Error al subir el archivo";
                }
            } 
            else {
                $status2 = "Error al subir archivo";
            }

        } // fin de la condición if ($vacioPDF!="" or $vacioImagen!="") 
        else{
            
            echo "<script languaje='javascript'>alert('Aun falta subir el archivo PDF')</script>";
        }

        } //fin de la condición if ($_POST["action"] == "upload")              
        }
        
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador</title>
        <link href="css/csslogin.css" rel="stylesheet">
        <LINK REL="Shortcut Icon" HREF="images/icono.png">
        <link href="css/metro-bootstrap.css" rel="stylesheet">
        <link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
        <link href="js/prettify/prettify.css" rel="stylesheet">

        <!-- Load JavaScript Libraries -->
        <script src="js/jquery/jquery.min.js"></script>
        <script src="js/jquery/jquery.widget.min.js"></script>
        <script src="js/jquery/jquery.mousewheel.js"></script>
        <script src="js/prettify/prettify.js"></script>
        <script src="js/metro.min.js"></script>

        <link rel="stylesheet" type="text/css" href="css/cssbook.css"> 

    </head>
    <body class="metro">

<div id="content_page">
            <nav class="navigation-bar dark">
                <div class="navigation-bar-content">
                    <a href="admin.php" class="element"><span class="icon-grid-view"></span> Digitial Books <sup>1.0</sup></a>
                    <span class="element-divider"></span>
                    <a class="pull-menu" href="#"></a>
                    <ul class="element-menu">
                        <li>
                            <a class="dropdown-toggle" href="#">Acerca de</a>
                            <ul class="dropdown-menu dark" data-role="dropdown">
                                <li><a href="acerca.php">Acerca de</a></li>
                                <li><a href="programadores.php">Programadores</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="no-tablet-portrait">
                        <span class="element-divider"></span>
                        <a class="element brand" href="bookAlta.php"><span class="icon-spin"></span></a>
                        <span class="element-divider"></span>
                        <div class="element place-right">
                            <a class="dropdown-toggle" href="#">
                                <span class="icon-cog"></span>
                            </a>
                            <ul class="dropdown-menu place-right" data-role="dropdown" style="display: none;">
                                <li><a href="#">Cuenta</a></li>
                                <li><a href="#">Cambiar Nombre</a></li>
                                <li><a href="cerrarsesion.php">Salir</a></li>
                            </ul>
                        </div>
                        <span class="element-divider place-right"></span>
                        <button class="element image-button image-left place-right">
                            <?php echo $_SESSION["nomuser"];?>
                            <img src="images/me.jpg">
                        </button>
                    </div>
                </div>
            </nav>      
            <div class="row">
                <div class="column grid_3">
                    <ul class="dropdown-menu dark inverse open keep-open" style="position: relative; width: 200px; z-index: 1">
                        <li class="menu-title">Funciones</li>
                        <li><a href="bookAlta.php">Agregar Libros</a></li>
                        <li><a href="eliminar.php">Eliminar Libros</a></li>
                        <li><a href="Consultar.php">Buscar Libros</a></li>
                    </ul>
                </div>
                <div class="column grid_9">
                    <div class="alig-lib-rev">
                        <div class="balloon right">
                            <div class="tab-control padding20" data-role="tab-control">
                                <ul class="tabs">
                                    <li class="active   "><a href="#write">Información del nuevo libro</a></li>
                                </ul>
                                <div class="frames">
                                    <div id="write" class="frame" style="display: none;">
                                        <form action="bookAlta.php" method="post" name="miForm" enctype="multipart/form-data">
                                            <table border="0" width="100%" cellpadding="10" align="center">
                                                <tr><td>Nombre: </td>    <td><input type="text" id="nombre" name="nombre" /></td>
                                                    <td>Autor: </td>     <td><input type="text" id="autor" name="autor" /></td></tr>
                                                <tr><td>Edición: </td>   <td><input type="text" id="edicion" name="edicion" /></td>
                                                    <td>ISBN: </td>      <td><input type="text" id="isbn" name="isbn" /></td></tr>
                                                <tr><td>Editorial: </td> <td><input type="text" id="editorial" name="editorial" /></td>
                                                    <td>Categoría: </td>
                                                    <td><select id="categoria" name="categoria">";
                                                          <option>HISTORIA</option>
                                                          <option>FILOSOFÍA</option>
                                                          <option>INFORMÁTICA</option>
                                                          <option>ENFERMERÍA</option>
                                                          <option>NUTRICIÓN</option>
                                                          <option>ADMINISTRACIÓN</option>
                                                    </select>
                                                </td></tr>
                                            </table>
                                            <table border="0" width="100%" cellpadding="10" align="center">
                                                <tr>
                                                <td>Seleccione libro<br><input name="archivo" type="file" id="archivo" class="boton" size="35" /></td></tr>
                                                <tr>
                                                <tr>
                                                <td>Seleccione imagen<br><input name="imagen" type="file" id="imagen" class="boton" size="35" /></td>
                                                    <td><center><br><input name="enviar" type="submit" id="enviar" value="Agregar" /></center></td>
                                                    <td><center><br><input name="action" type="hidden" value="upload" /><input type="reset" name="Submit" value="Limpiar"></center></td></tr>
                                            </table>
                                        </form>
                                        <center><br><b>Libros cargados actualmente</b></center><br><br>
                                        <b>* Libros revisados</b>
                                        <?php 
                                            $conexion = mysql_connect(HOST, USERNAME,PASSWORD) or die("No se pudo conectar con el servidor");
                                            mysql_select_db("digitalbooks", $conexion);
                                            $query = "SELECT isbn, nombre from books where revisado=1";
                                            $res = mysql_query($query, $conexion) or die(mysql_error());
                                            while ($row = mysql_fetch_assoc($res)) {
                                                $nombre_fichero = 'img/'.$row['isbn'].'.jpg';
                                                if (file_exists($nombre_fichero)) {
                                                    echo "<center><img src=\"img/".$row['isbn'].".jpg"."\" width=\"50px\" height=\"50px\"></center>";
                                                }else{
                                                    echo "<center><img src=\"img/pdficon.png"."\" width=\"50px\" height=\"50px\"></center>";
                                                }
                                                echo "<center><a href=\"files/".$row['isbn'].".pdf"."\"  target=\"_new\">".$row['nombre']."</a></center>";      
                                                echo "<hr>";
                                            }
                                            mysql_close($conexion);
                                        ?>
                                        <b><br>* Libros no revisados</b>
                                        <?php 
                                            $conexion = mysql_connect(HOST, USERNAME,PASSWORD) or die("No se pudo conectar con el servidor");
                                            mysql_select_db("digitalbooks", $conexion);
                                            $query = "SELECT isbn, nombre from books where revisado=0";
                                            $res = mysql_query($query, $conexion) or die(mysql_error());
                                            while ($row = mysql_fetch_assoc($res)) {
                                                $nombre_fichero = 'imagetemp/'.$row['isbn'].'.jpg';
                                                if (file_exists($nombre_fichero)) {
                                                    echo "<center><img src=\"imagetemp/".$row['isbn'].".jpg"."\" width=\"50px\" height=\"50px\"></center>";
                                                }else{
                                                    echo "<center><img src=\"imagetemp/pdficon.png"."\" width=\"50px\" height=\"50px\"></center>";
                                                }      
                                                echo "<center><a href=\"filestemp/".$row['isbn'].".pdf"."\" target=\"_new\">".$row['nombre']."</a></center>";      
                                                echo "<hr>";
                                            }
                                            mysql_close($conexion);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</body>
</html>
<?php }  ?>


