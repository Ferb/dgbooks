DROP database IF EXISTS digitalbooks;
create database digitalbooks;
use digitalbooks;

create table passwd(
	password char(8) unique primary key);

create table datosadmin(
	passworduser char(8) COLLATE utf8_bin,
	nombreuser varchar(8),
	nombre varchar(25),
	apellidop varchar(25),
	apellidom varchar(25),
	passper char(8),
	primary key (passworduser,nombreuser),
	foreign key (passper) references passwd(password));

create table books(
	nombre varchar(100),
	autor varchar(100),
	edicion int,
	isbn varchar(20),
	editorial varchar(50),
	categoria varchar(50),
	revisado int,
	primary key (isbn));
insert into passwd values('12345678');
