<?php
include("config.php"); /*Archivos de configuración de la bases de datos*/
?>
<html>
<head>
		<title>Envio de datos por formulario</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link REL="Shortcut Icon" HREF="images/icono.png">
        <link href="css/metro-bootstrap.css" rel="stylesheet">
        <link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
        <link href="js/prettify/prettify.css" rel="stylesheet">

        <!-- Load JavaScript Libraries -->
        <script src="js/jquery/jquery.min.js"></script>
        <script src="js/jquery/jquery.widget.min.js"></script>
        <script src="js/jquery/jquery.mousewheel.js"></script>
        <script src="js/prettify/prettify.js"></script>
        <script src="js/metro.min.js"></script>
        <script src="js/funciones.js"></script>
        <link rel="stylesheet" type="text/css" href="css/csslogin.css">
</head>
<body>
	<?php
	error_reporting(E_ALL ^ E_DEPRECATED);
    @session_start();
	/*SE PREGUNTA SI HAY DATOS EN EL POST*/
    if (isset($_GET["isbn"])){
        $isbn=$_GET["isbn"];
        $image=$_GET["img"];
        $pdf=$_GET["isbn"].".pdf";
        $conexion = mysql_connect(HOST, USERNAME,PASSWORD) or die("No se pudo conectar con el servidor");
        mysql_select_db("digitalbooks", $conexion) or die("</br> No se pudo conectar a la base de datos"); /*accede a la base de datos, en caso de que la conexión sea correcta*/

        $result=mysql_query("UPDATE books SET revisado=1 WHERE isbn='".$isbn."';");
        if ($result){
            if($image!="no"){
                //copy("/imagetemp/".$image, "/img/".$image);
                if (!copy("./imagetemp/".$image, "./img/".$image)) {
                    echo "Error al copiar archivo...\n";
                }
            }
            copy("./filestemp/".$pdf, "./files/".$pdf);
            header("Location: admin.php?revisar=si&isbn=".$isbn);
        }
    }
	?>
</body>
</html>
