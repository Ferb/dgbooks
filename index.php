<?php
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Iniciar Sesión-DB</title>
        <link REL="Shortcut Icon" HREF="images/icono.png">
        <link href="css/metro-bootstrap.css" rel="stylesheet">
        <link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
        <link href="js/prettify/prettify.css" rel="stylesheet">

        <!-- Load JavaScript Libraries -->
        <script src="js/jquery/jquery.min.js"></script>
        <script src="js/jquery/jquery.widget.min.js"></script>
        <script src="js/jquery/jquery.mousewheel.js"></script>
        <script src="js/prettify/prettify.js"></script>
        <script src="js/metro.min.js"></script>
        <script src="js/funciones.js"></script>
        <link rel="stylesheet" type="text/css" href="css/csslogin.css">
    </head>
    <body class="metro bg-body">
        <div class="content_page">
            <div class="row">
                <div class="column grid_12 bg-captura-user mar-top-content">
                    <div class="bg-slider-login">
                        <div class="mar-title-login">
                            <h2>
                                Inicio de Sesión<small class="on-right">Digital-Books</small>
                            </h2>
                        </div>
                        <div class="alig-bot-invitado">
                            <a href="invitado.php" class="button bg-pass button large primary">Invitado</a>
                        </div>
                        <hr>
                        <?php 
                            include("config.php"); /*Archivos de configuración de la bases de datos*/
                            if (isset($_GET["pass"])){
                                if ($_GET["pass"]=="si"){
                                    echo "<div class=\"color-pass-update\"><p>Contraseña actualizada.</p></div>";
                                }
                            }
                            if (isset($_GET["error"])){
                                if ($_GET["error"]=="si"){
                                    echo "<div class=\"color-error-sesion\"><p>Usuario o Contraseña Incorrecta.</p></div>";
                                }else{
                                    echo "<div class=\"color-error-sesion\"><p>Por favor inicie sesión.</p></div>";
                                }
                            }
                            if (isset($_GET["createuser"])){
                                if ($_GET["createuser"]=="si"){
                                    echo "<div class=\"color-pass-update\"><p>¡Usuario Creado! Ahora inicie sesión.</p></div>";
                                }
                            }
                        ?>
                        <div class="mar-form-login">
                            <form action="validar.php" method="post" onSubmit="return noUserPassVacios(this);" onKeyUp="calcLong('pass',this,20); calcLong('log',this, 20);">
                                <div class="tam-cont-input">
                                    <div class="input-control text">
                                        <input name="log" id="log" type="text" value="<?php if (isset($_GET["user"])){ echo $_GET["user"];}?>" placeholder="Nombre de Usuario" onkeypress="return validarNumLetras(event);"/>
                                        <button class="btn-clear"></button>
                                    </div>
                                    <div class="input-control password">
                                        <input name="pass" id="pass" type="password" value="" placeholder="Contraseña"/>
                                        <button class="btn-reveal"></button>
                                    </div>
                                    <div>
                                        <a href="nuevapasswd.php" class="button link bg-pass"><p>Recuperar Contraseña</p></a>
                                        <a href="registro.php" class="button link bg-registro"><p>Registrarse</p></a>
                                    </div>
                                </div>
                                <div class="alig-bot-admin">
                                    <input type="submit" name="Submit" value="Administrador" class="button bg-pass button large primary">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>